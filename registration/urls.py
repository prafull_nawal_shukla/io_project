from django.conf.urls import url, include
from registration.views import *

urlpatterns = [
    url(r'^reg-page/$', registration_page, name='reg-page'),
    url(r'^login-page/$', login_page, name='login-page'),
    url(r'^registration-user/$', register_user, name='registration-user'),
    url(r'^login-user/$', login_user, name='login-user'),

]
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField


class User(AbstractUser):
    registration_time = models.DateTimeField(null=True)
    ip = models.CharField(max_length=500, null=True)
    country = CountryField()


class UserBloking(models.Model):
    user = models.OneToOneField(User)
    count = models.IntegerField(default=0)
    blocking_time = models.DateTimeField(null=True)

# Create your models here.

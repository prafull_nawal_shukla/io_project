# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from registration.models import *
import json
from urllib2 import urlopen
from django.utils import timezone
import os
import re
# Create your views here.


def registration_page(request):
    try:
        return render(request, 'reg.html')
    except Exception as e:
        print(e)


def login_page(request):
    try:
        return render(request, 'login.html')
    except Exception as e:
        print(e)


def register_user(request):
    try:
        name = request.POST['name'].strip().split(' ')
        first_name = name[0]
        last_name = " ".join(name[1:])
        email = request.POST['email'].strip()
        password = request.POST['password'].strip()
        if not User.objects.filter(username=email).exists():
            url = 'http://ipinfo.io/json'
            response = urlopen(url)
            data = json.load(response)
            ip = data['ip']
            country = data['country']
            user = User.objects.create(username=email,
                                       email=email,
                                       first_name=first_name, last_name=last_name,
                                       registration_time=timezone.now(),
                                       ip=ip, country=country)
            user.set_password(password)
            user.save()

            UserBloking.objects.create(user_id=user.id)
            return HttpResponse("User Register Successfully")
        else:
            return HttpResponse("User Already register with Us")
    except Exception as e:
        return HttpResponse("Something Bad happened Please contact Administrator.")

def login_user(request):
    try:
        time_diff = 0
        user_name = request.POST['email'].strip()
        password = request.POST['password'].strip()
        if user_name and password is not None:
            user_obj = User.objects.filter(username=user_name)
            if user_obj:
                # check blocking time first for 5 min
                if user_obj.first().userbloking.count >= 3:
                    time_diff = (timezone.now() - user_obj.first().userbloking.blocking_time).total_seconds() / 60


                if time_diff > 5 or time_diff == 0:
                    user = authenticate(username=user_name, password=password)
                    if user is not None:
                        user.userbloking.count = 0
                        user.userbloking.blocking_time = None
                        user.save()
                        return render(request, 'dashboard.html', {'f_name':user.first_name, 'l_name':user.last_name,'ip': user.ip, 'country':user.country})
                    else:
                        user_block_obj = UserBloking.objects.filter(user_id=user_obj[0].id)
                        count = user_block_obj.first().count
                        count += 1
                        user_block_obj.update(count=count)
                        if count >= 3:
                            user_block_obj.update(blocking_time=timezone.now())
                        return HttpResponse("Username or password is incorrect")
                else:
                    msg = "Please wait for {} to login again".format(5-time_diff)
                    return HttpResponse(msg)

            else:
                return HttpResponse("User Not Found")
        else:
            return HttpResponse("Please Enter User name and Password")
    except Exception as e:
        print e
        return HttpResponse("Something Bad happened Please contact Administrator.")